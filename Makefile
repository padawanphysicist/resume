RESUME=victor_santos
PACKAGE=http://lmtx.pragma-ade.nl/install-lmtx/context-linux-64.zip
DESTDIR=$$HOME/bin/context

all: en pt clean

en: dist/victor-santos-1page-en.pdf dist/victor-santos-2page-en.pdf
pt: dist/victor-santos-1page-pt_br.pdf dist/victor-santos-2page-pt_br.pdf

.PHONY: dist
dist:
	@mkdir --parents dist

dist/victor-santos-2page-en.pdf: dist resume.tex
	@context --purgeall --mode=twopage,english resume.tex --result=$(notdir $@)
	@mv $(notdir $@) dist

dist/victor-santos-2page-pt_br.pdf: dist resume.tex
	@context --purgeall --mode=twopage,portuguese resume.tex --result=$(notdir $@)
	@mv $(notdir $@) dist

dist/victor-santos-1page-en.pdf: dist resume.tex
	@context --purgeall --mode=onepage,english resume.tex --result=$(notdir $@)
	@mv $(notdir $@) dist

dist/victor-santos-1page-pt_br.pdf: dist resume.tex
	@context --purgeall --mode=onepage,portuguese resume.tex --result=$(notdir $@)
	@mv $(notdir $@) dist

.PHONY: clean clean-all
clean:
	rm -rf *.log *.tuc
clean-all:
	rm -rf dist

docker: Dockerfile
	docker build -t resume .
