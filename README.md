My resume template using [ConTeXt](https://wiki.contextgarden.net/Main_Page) typesetting system.

# Install ConTeXt

Instructions from https://wiki.contextgarden.net/Installation:

- `mkdir $HOME/context && cd $HOME/context`
- `wget https://lmtx.pragma-ade.com/install-lmtx/context-linux-64.zip`
- `unzip context-linux-64.zip`
- `sh install.sh`
- `echo 'export PATH=$HOME/context/tex/texmf-linux-64/bin:$PATH' >> ~/.bashrc`
- `mtxrun --generate`
- `rm context-linux-64.zip`

# Fonts

Install the font files, add system font directory to OSFONTDIR:

- `export OSFONTDIR=$HOME/.local/share/fonts:/usr/share/fonts >> ~/.bashrc`
- `mtxrun --generate`
- `mtxrun --script fonts --reload`
