FROM debian:stable

ENV USER=docker
ENV GROUPNAME=$USER
ENV UID=12345
ENV GID=6789

RUN addgroup \
    --gid "$GID" \
    "$GROUPNAME" \
&&  adduser \
    --disabled-password \
    --gecos "" \
    --home "/home/$USER" \
    --ingroup "$GROUPNAME" \
    --uid "$UID" \
    $USER

RUN apt-get update \
&&  apt-get -y install wget unzip # texlive-luatex

WORKDIR /home/$USER
USER $USER

env PATH="/home/$USER/context/tex/texmf-linux-64/bin:$PATH"
RUN mkdir context && cd context &&  wget https://lmtx.pragma-ade.com/install-lmtx/context-linux-64.zip &&  unzip context-linux-64.zip &&  bash install.sh && mtxrun --generate &&  rm context-linux-64.zip


#RUN mkdir context && cd context &&  wget https://lmtx.pragma-ade.com/install-lmtx/context-linux-64.zip &&  unzip context-linux-64.zip &&  bash install.sh && mtxrun --generate ##&&  context --luatex --generate &&  context --luatex --make 

#&&  rm context-linux-64.zip

ENTRYPOINT ["context"]
